package HW3;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.json.simple.JSONObject;


/**
 * 
 * JsonAdd.java
 * 
 * Purpose: To write JSON file, witch will be put to logstash 
 *
 * @author Natatya Galkina
 * @version 1.0 16/12/17
 */

public class JsonAdd {
	/**
	 *   It puts data to JSON file, , witch will be put to logstash                        
	 * <p>
	 * Reads each entry of given Map. Puts it into Json object and save in file
	 *    
	 *
	 * @param  Map data - Map of Cuont per Geo
	 * @return void.
	 */
    public void add(Map data) {
    	
    	/**
	 	* Create a test.json file
	 	*/ 
    	try (FileWriter file = new FileWriter("test.json")) {
    		
        /**
    	 	* Create an Iterator
    	 	*/   		
    	Iterator<Map.Entry<String, Integer>> entries = data.entrySet().iterator();
    	
    	/**
	 	* Repeat for all entries
	 	*/ 
    	while (entries.hasNext()) {
            Map.Entry<String, Integer> entry = entries.next();
        	/**
    	 	* Create  json object
    	 	*/ 
            JSONObject obj = new JSONObject();
        	/**
    	 	* Put data to json
    	 	*/ 
            obj.put("Count", entry.getValue());
            obj.put("Geo", entry.getKey());
        	/**
    	 	* Write to file
    	 	*/ 
            file.write(obj.toJSONString()+"\n");
            file.flush();
        	/**
    	 	* Write to console
    	 	*/ 
            System.out.println(obj);
        }
    	   

        } catch (IOException e) {
            e.printStackTrace();
        }

      

    }

}
