package HW3;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import twitter4j.*;
import twitter4j.conf.ConfigurationBuilder;
/**
 * 
 * Twitter.java
 * 
 * Purpose: To get twits 
 *
 * @author Natatya Galkina
 * @version 1.0 16/12/17
 */
public class Twitter {
	/**
	 *   It gets twits                       
	 * <p>
	 * Connects to Twitter.Listens for twits. Write it into JSON? using JsonAdd.java
	 *    
	 *
	 * @param  no
	 * @return void.
	 */
	public static void main(String[] args) {
		
    	/**
	 	* Creates configuration
	 	*/
		 ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
	    	/**
		 	* Set Keys and Secrets
		 	*/
	        configurationBuilder.setOAuthConsumerKey("AOv0KoC2zLc0rNOG80GNywoEP")
	                .setOAuthConsumerSecret("gZiMUQsqUwEVo1GPLYjMqx3saNVouHvOApeIzVUzcrhMSEyJUr")
	                .setOAuthAccessToken("941626249771995136-ZTCXgpTNdRJqcfl5Pt4QTRanR0lhrIz")
	                .setOAuthAccessTokenSecret("BZ4KryB03jg2NO3N7I0gdNwbF26USKjbsRWSn2oJnKFG0");
	        
	        
	        
	    	/**
		 	* Create an instance of JsonAdd 
		 	*/
  JsonAdd js = new JsonAdd();
	/**
	* Create a Map for results
	*/
  Map data = new HashMap< String, Integer>();
  
  
	/**
	* Create twitterStream instance
	*/
TwitterStream twitterStream = new TwitterStreamFactory(configurationBuilder.build()).getInstance();  


	/**
	* Set a Listener for twits
	*/
twitterStream.addListener(new StatusListener () {
	
	/**
 	* Catch new twit
 	*/
    public void onStatus(Status status) {
    	/**
	 	* Limit amount of twits
	 	*/
    	if (data.size() < 20){
        	/**
    	 	* Get Geo
    	 	*/	
    	String country =  status.getPlace().getCountry();
    	/**
	 	* Do we have some geo?
	 	*/
    	if (!country.equals(null)){
        	/**
    	 	* Console output
    	 	*/
       System.out.println(status.getPlace().getCountry()+"##########"+ status.getText()); // print tweet text to console
   			/**
   			 * Have we already got this country?
   			 */
       		if(!data.containsKey(status.getPlace().getCountry())){
       	    	/**
       		 	* If not, put it
       		 	*/
    			data.put(status.getPlace().getCountry(), 1);
    		}else{
    	    	/**
    		 	* If we had, look at the count and implement it
    		 	*/
    			int N = (int) data.get(country);
    			N++;
    	    	/**
    		 	* Update data
    		 	*/
    			data.put(country, N);
    			
    		}
    	
    }}
    	else {
        	/**
    	 	* Write JSON 
    	 	*/
    		js.add(data);
        	/**
    	 	* Shutdown stream
    	 	*/
    		twitterStream.shutdown();
    
        }
         
    }
 
	@Override
	public void onException(Exception arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onDeletionNotice(StatusDeletionNotice arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onScrubGeo(long arg0, long arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStallWarning(StallWarning arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTrackLimitationNotice(int arg0) {
		// TODO Auto-generated method stub
		
	}

});

	/**
	* Run the Stream
	*/
twitterStream.sample();	
	
	        
	        
	}}
